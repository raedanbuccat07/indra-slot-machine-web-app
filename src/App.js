import { useState, useEffect, Fragment, useCallback, useRef } from "react";
import { Container, Row, Col, Button, Modal, Table, Accordion, Form, Tab, Tabs, Image } from "react-bootstrap";
import Swal from "sweetalert2/dist/sweetalert2.all.min.js";
import { Howl } from "howler";
import Slot from "./audioclips/Slot.mp3";
import Slot3 from "./audioclips/Slot3.mp3";
import Lever from "./audioclips/Lever.mp3";
import backgroundDefault from "./images/DefaultBackground.png";
import defaultBackground from "./images/DefaultBackground.png";
import ReactCanvasConfetti from "react-canvas-confetti";
import * as xlsx from "xlsx";
import "./App.css";
import { Scrollbars } from "react-custom-scrollbars-2";
import { RadialTextGradient } from "react-text-gradients-and-animations";

import SlotMachineLogo from './images/9b870687-49ff-422c-808d-14861451191c.png';

//slots
import SlotCounter from 'react-slot-counter';
import Sparkle from 'react-sparkle'
import 'animate.css';

const canvasStyles = {
  position: "fixed",
  pointerEvents: "none",
  width: "100%",
  height: "100%",
  top: 0,
  left: 0,
  zIndex: 1100,
};

function App() {
  const [randomizerData, setRandomizerData] = useState([]);
  const [randomizerUsed, setRandomizerUsed] = useState([]);
  const [randomizerDataOriginal, setRandomizerDataOriginal] = useState([]);
  const [randomizerDisplay, setRandomizerDisplay] = useState([]);
  const [fullName, setFullName] = useState([]);
  const [oldDataDisplay, setOldDataDisplay] = useState("");
  const [excludedNumber, setExcludedNumber] = useState([]);
  const [showSecondDataText, setShowSecondDataText] = useState(false);
  const [modalShow, setModalShow] = useState(false);
  const [backgroundImage, setBackgroundImage] = useState("");
  const refAnimationInstance = useRef(null);
  const [arrayKeyNames, setArrayKeyNames] = useState([]);
  const [firstData, setFirstData] = useState();
  const [secondData, setSecondData] = useState();
  const [backgroundBlur, setBackgroundBlur] = useState(0);
  //slot machine lever
  const [leverPulled, setLeverPulled] = useState(false);

  const getInstance = useCallback((instance) => {
    refAnimationInstance.current = instance;
  }, []);

  const makeShot = useCallback((particleRatio, opts) => {
    refAnimationInstance.current &&
      refAnimationInstance.current({
        ...opts,
        origin: { y: 0.7 },
        particleCount: Math.floor(200 * particleRatio),
      });
  }, []);

  const fire = useCallback(() => {
    makeShot(0.25, {
      spread: 26,
      startVelocity: 55,
    });

    makeShot(0.2, {
      spread: 60,
    });

    makeShot(0.35, {
      spread: 100,
      decay: 0.91,
      scalar: 0.8,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 25,
      decay: 0.92,
      scalar: 1.2,
    });

    makeShot(0.1, {
      spread: 120,
      startVelocity: 45,
    });
  }, [makeShot]);

  // Employee Randomizer
  async function dataRandomizer(randomInt) {
    var oldDisplay = randomizerDisplay[firstData];
    setRandomizerDisplay(randomizerData[randomInt]);

    if (randomizerData[randomInt][secondData] !== undefined) {
      setFullName(randomizerData[randomInt][secondData].split(" "));
    } else {
      setFullName("EMPTY NAME".split(" "));
    }

    setRandomizerUsed([...randomizerUsed, randomizerData[randomInt]]);

    if (oldDisplay !== randomizerDisplay[firstData]) {
      setOldDataDisplay(oldDisplay);
    }

    SoundPlay(Slot);
  };

  const drawRandomizerData = async () => {
    let randomInt = Math.floor(Math.random() * randomizerData.length);

    if (firstData?.length === undefined || secondData.length === undefined) {
      Swal.fire({
        icon: "error",
        title: "Select Data",
        text: "Please select data in the options before starting.",
      });

      return;
    };

    if (randomizerData?.length === 0) {
      Swal.fire({
        icon: "error",
        title: "Randomizer Data List Empty",
        text: "Please upload excel data in the options before starting.",
      });

      return;
    } else {
      do {
        if (excludedNumber.includes(randomInt)) {
          randomInt = Math.floor(Math.random() * randomizerData.length);
        }

        if (excludedNumber.length === randomizerData.length) {
          Swal.fire({
            icon: "error",
            title: "Reset",
            text: "All randomizer data has appeared. Please reset the list.",
          });
          setLeverPulled(false);
          return;
        }

        if (!excludedNumber.includes(randomInt)) {
          setExcludedNumber([...excludedNumber, randomInt]);
          dataRandomizer(randomInt);
        }
      } while (excludedNumber.includes(randomInt));
    }
    const timer = (ms) => new Promise((res) => setTimeout(res, ms));
    counterRef.current?.startAnimation({
      duration: 5,
      dummyCharacterCount: 200,
      direction: 'top-down',
    });

    await timer(5200);

    showData();
  };

  const winnerList = async () => {
    setModalShow(true);
  };

  const employeeReset = async () => {
    Swal.fire({
      title: "Are you sure you want to reset?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "RESET",
    }).then((result) => {
      if (result.isConfirmed) {
        setRandomizerData(randomizerDataOriginal);
        setExcludedNumber([]);
        setRandomizerDisplay([]);
        setRandomizerUsed([]);
        setShowSecondDataText(false);
        setFullName([]);
        setOldDataDisplay("");
        setModalShow(false);
        setArrayKeyNames([]);
        setLeverPulled(false);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Reset",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const showData = async () => {
    fire();
    SoundPlay(Slot3);
    setShowSecondDataText(true);
  };

  const readUploadFile = async (e) => {
    e.preventDefault();

    if (e.target.files) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target.result;
        const workbook = xlsx.read(data, { type: "array" });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = xlsx.utils.sheet_to_json(worksheet);

        let tempData = json;
        let dataKeyNames = Object.keys(tempData[0]);

        setArrayKeyNames(dataKeyNames);

        tempData.map((data, i) => {
          return (tempData[i].isUsed = false);
        });

        setRandomizerData(tempData);
        setRandomizerDataOriginal(tempData);

        Swal.fire({
          position: "center",
          icon: "success",
          title: "Uploaded Data",
          showConfirmButton: false,
          timer: 1500,
        });

        document.getElementById("uploadData").value = "";
      };
      reader.readAsArrayBuffer(e.target.files[0]);
    }
  };

  const SoundPlay = (src) => {
    const sound = new Howl({
      src,
      html5: true,
    });
    sound.play();
  };

  const checkboxClick = (data, type) => {
    switch (type) {
      case 1:
        setFirstData(data);
        break;
      case 2:
        setSecondData(data);
        break;
      default:
    }
  };

  const addBlurBackground = () => {
    setBackgroundBlur(backgroundBlur + 1);
  };

  const reduceBlurBackground = () => {
    if (backgroundBlur <= 0) {
      return
    } else {
      setBackgroundBlur(backgroundBlur - 1);
    }
  };

  const removeBlurBackground = () => {
    setBackgroundBlur(0);
  };

  const setDefaultBackground = () => {
    setBackgroundImage(defaultBackground);
  };

  const removeBackground = () => {
    setBackgroundImage();
  };

  const counterRef = useRef(null);

  const pullLever = () => {
    if (leverPulled) {
      return; // Prevent pulling the lever when the animation is ongoing
    }

    setLeverPulled(true);
    SoundPlay(Lever);
    // Simulate some slot machine logic or fetch results here
    setTimeout(() => {
      setLeverPulled(false);
    }, 5400); // Reset the lever after 1 second (adjust as needed)

    drawRandomizerData();
    setShowSecondDataText(false);
  };

  useEffect(() => {
    const fileInput = document.getElementById("fileInput");
    fileInput?.addEventListener("change", (e) => {
      const file = fileInput?.files[0];
      const reader = new FileReader();
      reader?.addEventListener("load", () => {
        setBackgroundImage(reader?.result);
      });
      if (file) {
        reader?.readAsDataURL(file);
      }
      document.getElementById("fileInput").value = "";
    });
  }, [backgroundBlur]);

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
      >
        {
          leverPulled ?
            (<Sparkle
              minSize={10}
              maxSize={30}
              flickerSpeed={"fast"}
              overflowPx={-10}
            />)
            :
            (<Sparkle
              minSize={1}
              maxSize={10}
              overflowPx={-10}
            />)
        }
        <div className="lottery-container2 w-100 h-100" style={{ backgroundImage: `url(${backgroundImage ? backgroundImage : backgroundDefault})`, filter: `blur(${backgroundBlur}px)`, WebkitFilter: `blur(${backgroundBlur}px)` }}></div>
        <Container fluid className="lottery-container h-100 m-0 p-0">
          <Container fluid className="reel-background h-100 m-0 p-0">
            <Container fluid className="reel-background2 h-100 m-0 p-0 w-100 ">
            </Container>
            <Row className="h-25 m-0 p-0 w-100 d-flex align-items-center justify-content-center p-4">

            </Row>
            <Row className="h-50 w-100 m-0 p-0">
              <Col className="h-100 m-0 p-0 d-flex align-items-center justify-content-end w-75 ms-5">
                <Row className="h-100 m-0 p-0 w-80 d-flex align-items-center justify-content-center ms-5 mt-5">
                  <Col className="reel-container d-flex align-items-center justify-content-center col-8 h-80 mt-2 m-0 p-0 w-70 pe-3">
                    {randomizerDisplay ? (
                      <Fragment>
                        <SlotCounter
                          valueClassName="machine-value"
                          charClassName="machine-char"
                          separatorClassName="machine-sep"
                          ref={counterRef}
                          startValue={oldDataDisplay.length !== 0 ? '777777' : oldDataDisplay.toString().slice(0, 6).padEnd(6, '7')}
                          value={randomizerDisplay[firstData] ? randomizerDisplay[firstData].toString().slice(0, 6).padEnd(6, '7') : '777777'}
                          animateUnchanged
                          hasInfiniteList
                          direction="bottom-up"
                          autoAnimationStart={false}

                        />
                      </Fragment>
                    ) : (
                      <></>
                    )}
                  </Col>
                  <Col className="d-flex align-items-center justify-content-start col-4 h-75 m-0 p-0 w-30">
                    <div className={`slot-machine-lever ${leverPulled ? 'lever-pulled' : ''} w-100 m-0 h-50`} onClick={pullLever}>
                      <div className="slot-machine-lever-handle m-0 p-0 h-100 w-100"></div>
                    </div>
                  </Col>
                </Row>

                <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />
              </Col>
            </Row>
            <Row className="h-25 m-0 p-0 w-100 d-flex align-items-center justify-content-center">
              {
                showSecondDataText ? (
                  <Fragment>
                    <Col xs={12} className="m-0 p-0 h-100 d-flex align-items-center justify-content-center">
                      <Container fluid className="h-40 w-auto name-box d-flex align-items-center justify-content-center animate__animated animate__zoomIn animate__fast">
                        <span className="text-randomizer-name text-uppercase">
                          {" "}
                          <RadialTextGradient
                            shape={"circle"}
                            position={"center"}
                            colors={["orange", "yellow", "green", "violet"]}
                            animate={true}
                            animateDirection={"horizontal"}
                            animateDuration={10}
                          >
                            {fullName.map((data, index) => {
                              return (
                                <Fragment key={index}>
                                  {data + " "}
                                </Fragment>
                              )
                            })}
                          </RadialTextGradient>
                        </span>
                      </Container>
                    </Col>
                  </Fragment>
                ) : (
                  <Col xs={12} className="m-0 p-0 h-100 w-100 d-flex align-items-center justify-content-center">
                    <Image src={SlotMachineLogo} className="logo-slot-machine animate__animated animate__zoomInDown animate__fast" />
                  </Col>
                )
              }

            </Row>
          </Container>
        </Container>
        <Accordion>
          <Accordion.Item eventKey="0">
            <Accordion.Header>OPTIONS (CLICK ME)</Accordion.Header>
            <Accordion.Body>
              <Tabs defaultActiveKey="file" className="mb-3">
                <Tab eventKey="file" title="Upload File">
                  <Row>
                    <Col className="text-end">
                      <form>
                        <label
                          htmlFor="uploadData"
                          style={{
                            cursor: "pointer",
                            background: "grey",
                            padding: "5px",
                            color: "white",
                          }}
                        >
                          Upload Form Data
                        </label>
                        <input
                          className="d-none"
                          style={{ visibility: "hidden" }}
                          type="file"
                          name="uploadData"
                          id="uploadData"
                          onChange={readUploadFile}
                        />
                      </form>
                    </Col>
                    <Col className="text-start">
                      <form>
                        <label
                          htmlFor="fileInput"
                          style={{
                            cursor: "pointer",
                            background: "grey",
                            padding: "5px",
                            color: "white",
                          }}
                        >
                          Set Background Image
                        </label>
                        <input
                          className="d-none"
                          style={{ visibility: "hidden" }}
                          type="file"
                          name="fileInput"
                          id="fileInput"
                        />
                      </form>
                    </Col>
                    <Col className="text-start">
                      <Button variant="info" onClick={() => setDefaultBackground()}>Add Default Background</Button>
                    </Col>
                    <Col className="text-start">
                      <Button variant="dark" onClick={() => removeBackground()}>Remove Background</Button>
                    </Col>
                    <Col className="text-start">
                      <Button variant="success" onClick={() => addBlurBackground()}>Add Blur</Button>
                      <Button variant="primary" onClick={() => reduceBlurBackground()}>Reduce Blur</Button>
                      <Button variant="danger" onClick={() => removeBlurBackground()}>Remove Blur</Button>
                    </Col>
                  </Row>
                </Tab>
                <Tab eventKey="data" title="Select Data (IMPORTANT)">
                  <Row className="m-0 p-0">
                    <Col xs={12} className="radio-section">
                      {arrayKeyNames?.length !== 0 ? (
                        <Form className="d-flex justify-content-center ">
                          <div className="m-3 text-center">
                            <h3>1st Data: </h3>
                            {arrayKeyNames?.map((data) => {
                              return (
                                <Form.Check
                                  inline
                                  label={data}
                                  name="group1"
                                  type="radio"
                                  id={`inline-${data}`}
                                  key={data}
                                  onClick={() => checkboxClick(data, 1)}
                                />
                              );
                            })}
                          </div>
                        </Form>
                      ) : (
                        <h3 className="text-center">Empty Data</h3>
                      )}
                    </Col>
                    <Col xs={12} className="radio-section">
                      {arrayKeyNames?.length !== 0 ? (
                        <Form className="d-flex justify-content-center">
                          <div className="m-3 text-center">
                            <h3>2nd Data: </h3>
                            {arrayKeyNames?.map((data) => {
                              return (
                                <Form.Check
                                  inline
                                  label={data}
                                  name="group2"
                                  type="radio"
                                  id={`inline-${data}`}
                                  key={data}
                                  onClick={() => checkboxClick(data, 2)}
                                />
                              );
                            })}
                          </div>
                        </Form>
                      ) : (
                        <></>
                      )}
                    </Col>
                  </Row>
                </Tab>
                <Tab eventKey="drawlist" title="Draw List/Reset">
                  <Row>
                    <Col className="text-end">
                      <Button
                        size="lg"
                        variant="light"
                        className="winner-list-button border-radius-button"
                        onClick={() => winnerList()}
                        active
                      >
                        DRAW LIST
                      </Button>
                    </Col>
                    <Col className="text-start">
                      <Button
                        size="lg"
                        variant="light"
                        className="reset-button border-radius-button"
                        onClick={() => employeeReset()}
                        active
                      >
                        RESET
                      </Button>
                    </Col>
                  </Row>
                </Tab>
              </Tabs>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </Scrollbars >
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <h1 className="text-center">
              Raffle Draw List - Total Draw: {randomizerUsed.length}
            </h1>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>{firstData}</th>
                <th>{secondData}</th>
              </tr>
            </thead>
            <tbody>
              {randomizerUsed.length > 0 ? (
                randomizerUsed?.map((data, i) => (
                  <tr key={i}>
                    <td>
                      {i + 1} - {data[firstData]}
                    </td>
                    <td>{data[secondData]}</td>
                  </tr>
                ))
              ) : (
                <></>
              )}
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
    </Fragment >
  );
}

export default App;
