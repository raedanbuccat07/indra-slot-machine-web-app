import React, { useState } from 'react';
import './SlotMachine.css';

const SlotMachine = () => {
  const [leverPulled, setLeverPulled] = useState(false);

  const pullLever = () => {
    if (leverPulled) {
      return; // Prevent pulling the lever when the animation is ongoing
    }

    setLeverPulled(true);

    // Simulate some slot machine logic or fetch results here
    setTimeout(() => {
      setLeverPulled(false);
    }, 1000); // Reset the lever after 1 second (adjust as needed)
  };

  return (
    <div className={`slot-machine-lever ${leverPulled ? 'lever-pulled' : ''}`} onClick={pullLever}>
      <div className="slot-machine-lever-handle"></div>
    </div>
  );
};

export default SlotMachine;
